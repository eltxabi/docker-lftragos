--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg90+1)
-- Dumped by pg_dump version 10.4 (Debian 10.4-2.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO txabi;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO txabi;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO txabi;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO txabi;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO txabi;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO txabi;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO txabi;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO txabi;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO txabi;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO txabi;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO txabi;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO txabi;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO txabi;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO txabi;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO txabi;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO txabi;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO txabi;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO txabi;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO txabi;

--
-- Name: lftragos_alineacion; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_alineacion (
    id integer NOT NULL
);


ALTER TABLE public.lftragos_alineacion OWNER TO txabi;

--
-- Name: lftragos_alineacion_equipo; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_alineacion_equipo (
    id integer NOT NULL,
    alineacion_id integer NOT NULL,
    equipo_id integer NOT NULL
);


ALTER TABLE public.lftragos_alineacion_equipo OWNER TO txabi;

--
-- Name: lftragos_alineacion_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_alineacion_equipo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_alineacion_equipo_id_seq OWNER TO txabi;

--
-- Name: lftragos_alineacion_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_alineacion_equipo_id_seq OWNED BY public.lftragos_alineacion_equipo.id;


--
-- Name: lftragos_alineacion_futbolista; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_alineacion_futbolista (
    id integer NOT NULL,
    alineacion_id integer NOT NULL,
    futbolista_id integer NOT NULL
);


ALTER TABLE public.lftragos_alineacion_futbolista OWNER TO txabi;

--
-- Name: lftragos_alineacion_futbolista_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_alineacion_futbolista_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_alineacion_futbolista_id_seq OWNER TO txabi;

--
-- Name: lftragos_alineacion_futbolista_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_alineacion_futbolista_id_seq OWNED BY public.lftragos_alineacion_futbolista.id;


--
-- Name: lftragos_alineacion_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_alineacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_alineacion_id_seq OWNER TO txabi;

--
-- Name: lftragos_alineacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_alineacion_id_seq OWNED BY public.lftragos_alineacion.id;


--
-- Name: lftragos_alineacion_jornada; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_alineacion_jornada (
    id integer NOT NULL,
    alineacion_id integer NOT NULL,
    jornada_id integer NOT NULL
);


ALTER TABLE public.lftragos_alineacion_jornada OWNER TO txabi;

--
-- Name: lftragos_alineacion_jornada_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_alineacion_jornada_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_alineacion_jornada_id_seq OWNER TO txabi;

--
-- Name: lftragos_alineacion_jornada_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_alineacion_jornada_id_seq OWNED BY public.lftragos_alineacion_jornada.id;


--
-- Name: lftragos_equipo; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_equipo (
    id integer NOT NULL,
    dinero integer NOT NULL,
    puntos_iniciales integer NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.lftragos_equipo OWNER TO txabi;

--
-- Name: lftragos_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_equipo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_equipo_id_seq OWNER TO txabi;

--
-- Name: lftragos_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_equipo_id_seq OWNED BY public.lftragos_equipo.id;


--
-- Name: lftragos_futbolista; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_futbolista (
    id integer NOT NULL,
    nombre character varying(30) NOT NULL,
    posicion character varying(3) NOT NULL,
    club character varying(20) NOT NULL,
    precio integer NOT NULL
);


ALTER TABLE public.lftragos_futbolista OWNER TO txabi;

--
-- Name: lftragos_futbolista_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_futbolista_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_futbolista_id_seq OWNER TO txabi;

--
-- Name: lftragos_futbolista_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_futbolista_id_seq OWNED BY public.lftragos_futbolista.id;


--
-- Name: lftragos_jornada; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_jornada (
    id integer NOT NULL,
    numero integer NOT NULL,
    limite timestamp with time zone NOT NULL
);


ALTER TABLE public.lftragos_jornada OWNER TO txabi;

--
-- Name: lftragos_jornada_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_jornada_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_jornada_id_seq OWNER TO txabi;

--
-- Name: lftragos_jornada_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_jornada_id_seq OWNED BY public.lftragos_jornada.id;


--
-- Name: lftragos_puntuacion; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_puntuacion (
    id integer NOT NULL,
    puntos integer
);


ALTER TABLE public.lftragos_puntuacion OWNER TO txabi;

--
-- Name: lftragos_puntuacion_futbolista; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_puntuacion_futbolista (
    id integer NOT NULL,
    puntuacion_id integer NOT NULL,
    futbolista_id integer NOT NULL
);


ALTER TABLE public.lftragos_puntuacion_futbolista OWNER TO txabi;

--
-- Name: lftragos_puntuacion_futbolista_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_puntuacion_futbolista_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_puntuacion_futbolista_id_seq OWNER TO txabi;

--
-- Name: lftragos_puntuacion_futbolista_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_puntuacion_futbolista_id_seq OWNED BY public.lftragos_puntuacion_futbolista.id;


--
-- Name: lftragos_puntuacion_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_puntuacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_puntuacion_id_seq OWNER TO txabi;

--
-- Name: lftragos_puntuacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_puntuacion_id_seq OWNED BY public.lftragos_puntuacion.id;


--
-- Name: lftragos_puntuacion_jornada; Type: TABLE; Schema: public; Owner: txabi
--

CREATE TABLE public.lftragos_puntuacion_jornada (
    id integer NOT NULL,
    puntuacion_id integer NOT NULL,
    jornada_id integer NOT NULL
);


ALTER TABLE public.lftragos_puntuacion_jornada OWNER TO txabi;

--
-- Name: lftragos_puntuacion_jornada_id_seq; Type: SEQUENCE; Schema: public; Owner: txabi
--

CREATE SEQUENCE public.lftragos_puntuacion_jornada_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lftragos_puntuacion_jornada_id_seq OWNER TO txabi;

--
-- Name: lftragos_puntuacion_jornada_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: txabi
--

ALTER SEQUENCE public.lftragos_puntuacion_jornada_id_seq OWNED BY public.lftragos_puntuacion_jornada.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: lftragos_alineacion id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion ALTER COLUMN id SET DEFAULT nextval('public.lftragos_alineacion_id_seq'::regclass);


--
-- Name: lftragos_alineacion_equipo id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_equipo ALTER COLUMN id SET DEFAULT nextval('public.lftragos_alineacion_equipo_id_seq'::regclass);


--
-- Name: lftragos_alineacion_futbolista id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_futbolista ALTER COLUMN id SET DEFAULT nextval('public.lftragos_alineacion_futbolista_id_seq'::regclass);


--
-- Name: lftragos_alineacion_jornada id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_jornada ALTER COLUMN id SET DEFAULT nextval('public.lftragos_alineacion_jornada_id_seq'::regclass);


--
-- Name: lftragos_equipo id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_equipo ALTER COLUMN id SET DEFAULT nextval('public.lftragos_equipo_id_seq'::regclass);


--
-- Name: lftragos_futbolista id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_futbolista ALTER COLUMN id SET DEFAULT nextval('public.lftragos_futbolista_id_seq'::regclass);


--
-- Name: lftragos_jornada id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_jornada ALTER COLUMN id SET DEFAULT nextval('public.lftragos_jornada_id_seq'::regclass);


--
-- Name: lftragos_puntuacion id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion ALTER COLUMN id SET DEFAULT nextval('public.lftragos_puntuacion_id_seq'::regclass);


--
-- Name: lftragos_puntuacion_futbolista id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_futbolista ALTER COLUMN id SET DEFAULT nextval('public.lftragos_puntuacion_futbolista_id_seq'::regclass);


--
-- Name: lftragos_puntuacion_jornada id; Type: DEFAULT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_jornada ALTER COLUMN id SET DEFAULT nextval('public.lftragos_puntuacion_jornada_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add equipo	7	add_equipo
20	Can change equipo	7	change_equipo
21	Can delete equipo	7	delete_equipo
22	Can add futbolista	8	add_futbolista
23	Can change futbolista	8	change_futbolista
24	Can delete futbolista	8	delete_futbolista
25	Can add jornada	9	add_jornada
26	Can change jornada	9	change_jornada
27	Can delete jornada	9	delete_jornada
28	Can add alineacion	10	add_alineacion
29	Can change alineacion	10	change_alineacion
30	Can delete alineacion	10	delete_alineacion
31	Can add puntuacion	11	add_puntuacion
32	Can change puntuacion	11	change_puntuacion
33	Can delete puntuacion	11	delete_puntuacion
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$20000$tn8d2A9nitT2$6pksWCJ4MOcOGb5qychRGz+hpFPa0WQf3Xznob0/A+0=	2018-08-30 22:05:39.470153+00	t	eltxabi			eltxabi@gmail.com	t	t	2018-08-10 20:53:01.353051+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	lftragos	equipo
8	lftragos	futbolista
9	lftragos	jornada
10	lftragos	alineacion
11	lftragos	puntuacion
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-08-09 23:50:33.145714+00
2	auth	0001_initial	2018-08-09 23:50:34.167714+00
3	admin	0001_initial	2018-08-09 23:50:34.369215+00
4	contenttypes	0002_remove_content_type_name	2018-08-09 23:50:34.445584+00
5	auth	0002_alter_permission_name_max_length	2018-08-09 23:50:34.490162+00
6	auth	0003_alter_user_email_max_length	2018-08-09 23:50:34.534442+00
7	auth	0004_alter_user_username_opts	2018-08-09 23:50:34.568539+00
8	auth	0005_alter_user_last_login_null	2018-08-09 23:50:34.600492+00
9	auth	0006_require_contenttypes_0002	2018-08-09 23:50:34.612105+00
10	lftragos	0001_initial	2018-08-09 23:50:36.042429+00
11	sessions	0001_initial	2018-08-09 23:50:36.250088+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
qvwcqwrbgw07j358odr5j5pfalc5d3cp	MzYwZDNiNDgyNTg0ZDFiNjdhODA0NjlmNmJkZTczZjYyYzIyMGM3Njp7Il9hdXRoX3VzZXJfaGFzaCI6ImQ3YzZhYjVjYzYwMDY4MjRiMDRlNmQ4YjhkYjQzYjM3N2M0ZTk2NzYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-09-13 22:05:39.493099+00
\.


--
-- Data for Name: lftragos_alineacion; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_alineacion (id) FROM stdin;
\.


--
-- Data for Name: lftragos_alineacion_equipo; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_alineacion_equipo (id, alineacion_id, equipo_id) FROM stdin;
\.


--
-- Data for Name: lftragos_alineacion_futbolista; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_alineacion_futbolista (id, alineacion_id, futbolista_id) FROM stdin;
\.


--
-- Data for Name: lftragos_alineacion_jornada; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_alineacion_jornada (id, alineacion_id, jornada_id) FROM stdin;
\.


--
-- Data for Name: lftragos_equipo; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_equipo (id, dinero, puntos_iniciales, usuario_id) FROM stdin;
\.


--
-- Data for Name: lftragos_futbolista; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_futbolista (id, nombre, posicion, club, precio) FROM stdin;
\.


--
-- Data for Name: lftragos_jornada; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_jornada (id, numero, limite) FROM stdin;
\.


--
-- Data for Name: lftragos_puntuacion; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_puntuacion (id, puntos) FROM stdin;
\.


--
-- Data for Name: lftragos_puntuacion_futbolista; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_puntuacion_futbolista (id, puntuacion_id, futbolista_id) FROM stdin;
\.


--
-- Data for Name: lftragos_puntuacion_jornada; Type: TABLE DATA; Schema: public; Owner: txabi
--

COPY public.lftragos_puntuacion_jornada (id, puntuacion_id, jornada_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 33, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 9, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 11, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 11, true);


--
-- Name: lftragos_alineacion_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_alineacion_equipo_id_seq', 1, false);


--
-- Name: lftragos_alineacion_futbolista_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_alineacion_futbolista_id_seq', 1, false);


--
-- Name: lftragos_alineacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_alineacion_id_seq', 1, false);


--
-- Name: lftragos_alineacion_jornada_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_alineacion_jornada_id_seq', 1, false);


--
-- Name: lftragos_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_equipo_id_seq', 1, false);


--
-- Name: lftragos_futbolista_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_futbolista_id_seq', 1, false);


--
-- Name: lftragos_jornada_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_jornada_id_seq', 1, false);


--
-- Name: lftragos_puntuacion_futbolista_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_puntuacion_futbolista_id_seq', 1, false);


--
-- Name: lftragos_puntuacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_puntuacion_id_seq', 1, false);


--
-- Name: lftragos_puntuacion_jornada_id_seq; Type: SEQUENCE SET; Schema: public; Owner: txabi
--

SELECT pg_catalog.setval('public.lftragos_puntuacion_jornada_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: lftragos_alineacion_equipo lftragos_alineacion_equipo_alineacion_id_equipo_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_equipo
    ADD CONSTRAINT lftragos_alineacion_equipo_alineacion_id_equipo_id_key UNIQUE (alineacion_id, equipo_id);


--
-- Name: lftragos_alineacion_equipo lftragos_alineacion_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_equipo
    ADD CONSTRAINT lftragos_alineacion_equipo_pkey PRIMARY KEY (id);


--
-- Name: lftragos_alineacion_futbolista lftragos_alineacion_futbolista_alineacion_id_futbolista_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_futbolista
    ADD CONSTRAINT lftragos_alineacion_futbolista_alineacion_id_futbolista_id_key UNIQUE (alineacion_id, futbolista_id);


--
-- Name: lftragos_alineacion_futbolista lftragos_alineacion_futbolista_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_futbolista
    ADD CONSTRAINT lftragos_alineacion_futbolista_pkey PRIMARY KEY (id);


--
-- Name: lftragos_alineacion_jornada lftragos_alineacion_jornada_alineacion_id_jornada_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_jornada
    ADD CONSTRAINT lftragos_alineacion_jornada_alineacion_id_jornada_id_key UNIQUE (alineacion_id, jornada_id);


--
-- Name: lftragos_alineacion_jornada lftragos_alineacion_jornada_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_jornada
    ADD CONSTRAINT lftragos_alineacion_jornada_pkey PRIMARY KEY (id);


--
-- Name: lftragos_alineacion lftragos_alineacion_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion
    ADD CONSTRAINT lftragos_alineacion_pkey PRIMARY KEY (id);


--
-- Name: lftragos_equipo lftragos_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_equipo
    ADD CONSTRAINT lftragos_equipo_pkey PRIMARY KEY (id);


--
-- Name: lftragos_equipo lftragos_equipo_usuario_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_equipo
    ADD CONSTRAINT lftragos_equipo_usuario_id_key UNIQUE (usuario_id);


--
-- Name: lftragos_futbolista lftragos_futbolista_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_futbolista
    ADD CONSTRAINT lftragos_futbolista_pkey PRIMARY KEY (id);


--
-- Name: lftragos_jornada lftragos_jornada_numero_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_jornada
    ADD CONSTRAINT lftragos_jornada_numero_key UNIQUE (numero);


--
-- Name: lftragos_jornada lftragos_jornada_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_jornada
    ADD CONSTRAINT lftragos_jornada_pkey PRIMARY KEY (id);


--
-- Name: lftragos_puntuacion_futbolista lftragos_puntuacion_futbolista_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_futbolista
    ADD CONSTRAINT lftragos_puntuacion_futbolista_pkey PRIMARY KEY (id);


--
-- Name: lftragos_puntuacion_futbolista lftragos_puntuacion_futbolista_puntuacion_id_futbolista_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_futbolista
    ADD CONSTRAINT lftragos_puntuacion_futbolista_puntuacion_id_futbolista_id_key UNIQUE (puntuacion_id, futbolista_id);


--
-- Name: lftragos_puntuacion_jornada lftragos_puntuacion_jornada_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_jornada
    ADD CONSTRAINT lftragos_puntuacion_jornada_pkey PRIMARY KEY (id);


--
-- Name: lftragos_puntuacion_jornada lftragos_puntuacion_jornada_puntuacion_id_jornada_id_key; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_jornada
    ADD CONSTRAINT lftragos_puntuacion_jornada_puntuacion_id_jornada_id_key UNIQUE (puntuacion_id, jornada_id);


--
-- Name: lftragos_puntuacion lftragos_puntuacion_pkey; Type: CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion
    ADD CONSTRAINT lftragos_puntuacion_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_group_permissions_0e939a4f ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_group_permissions_8373b171 ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_permission_417f1b1c ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_user_groups_0e939a4f ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_user_groups_e8701ad4 ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_user_user_permissions_8373b171 ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX django_admin_log_417f1b1c ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX django_admin_log_e8701ad4 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX django_session_de54fa62 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: lftragos_alineacion_equipo_6921bb5d; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_equipo_6921bb5d ON public.lftragos_alineacion_equipo USING btree (alineacion_id);


--
-- Name: lftragos_alineacion_equipo_f24bdbf2; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_equipo_f24bdbf2 ON public.lftragos_alineacion_equipo USING btree (equipo_id);


--
-- Name: lftragos_alineacion_futbolista_0716c32b; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_futbolista_0716c32b ON public.lftragos_alineacion_futbolista USING btree (futbolista_id);


--
-- Name: lftragos_alineacion_futbolista_6921bb5d; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_futbolista_6921bb5d ON public.lftragos_alineacion_futbolista USING btree (alineacion_id);


--
-- Name: lftragos_alineacion_jornada_05cbc893; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_jornada_05cbc893 ON public.lftragos_alineacion_jornada USING btree (jornada_id);


--
-- Name: lftragos_alineacion_jornada_6921bb5d; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_alineacion_jornada_6921bb5d ON public.lftragos_alineacion_jornada USING btree (alineacion_id);


--
-- Name: lftragos_puntuacion_futbolista_0716c32b; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_puntuacion_futbolista_0716c32b ON public.lftragos_puntuacion_futbolista USING btree (futbolista_id);


--
-- Name: lftragos_puntuacion_futbolista_83b5ca76; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_puntuacion_futbolista_83b5ca76 ON public.lftragos_puntuacion_futbolista USING btree (puntuacion_id);


--
-- Name: lftragos_puntuacion_jornada_05cbc893; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_puntuacion_jornada_05cbc893 ON public.lftragos_puntuacion_jornada USING btree (jornada_id);


--
-- Name: lftragos_puntuacion_jornada_83b5ca76; Type: INDEX; Schema: public; Owner: txabi
--

CREATE INDEX lftragos_puntuacion_jornada_83b5ca76 ON public.lftragos_puntuacion_jornada USING btree (puntuacion_id);


--
-- Name: auth_permission auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_jornada lftrag_alineacion_id_1382f6c31f362198_fk_lftragos_alineacion_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_jornada
    ADD CONSTRAINT lftrag_alineacion_id_1382f6c31f362198_fk_lftragos_alineacion_id FOREIGN KEY (alineacion_id) REFERENCES public.lftragos_alineacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_equipo lftrag_alineacion_id_5735ad2a46746143_fk_lftragos_alineacion_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_equipo
    ADD CONSTRAINT lftrag_alineacion_id_5735ad2a46746143_fk_lftragos_alineacion_id FOREIGN KEY (alineacion_id) REFERENCES public.lftragos_alineacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_futbolista lftrag_alineacion_id_655999f934e89e8f_fk_lftragos_alineacion_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_futbolista
    ADD CONSTRAINT lftrag_alineacion_id_655999f934e89e8f_fk_lftragos_alineacion_id FOREIGN KEY (alineacion_id) REFERENCES public.lftragos_alineacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_futbolista lftrag_futbolista_id_588dad76d009d5e1_fk_lftragos_futbolista_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_futbolista
    ADD CONSTRAINT lftrag_futbolista_id_588dad76d009d5e1_fk_lftragos_futbolista_id FOREIGN KEY (futbolista_id) REFERENCES public.lftragos_futbolista(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_puntuacion_futbolista lftrag_futbolista_id_6f3fd47184ded480_fk_lftragos_futbolista_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_futbolista
    ADD CONSTRAINT lftrag_futbolista_id_6f3fd47184ded480_fk_lftragos_futbolista_id FOREIGN KEY (futbolista_id) REFERENCES public.lftragos_futbolista(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_puntuacion_jornada lftrag_puntuacion_id_1f68a07c68979680_fk_lftragos_puntuacion_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_jornada
    ADD CONSTRAINT lftrag_puntuacion_id_1f68a07c68979680_fk_lftragos_puntuacion_id FOREIGN KEY (puntuacion_id) REFERENCES public.lftragos_puntuacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_puntuacion_futbolista lftrago_puntuacion_id_d70f3aea69ba867_fk_lftragos_puntuacion_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_futbolista
    ADD CONSTRAINT lftrago_puntuacion_id_d70f3aea69ba867_fk_lftragos_puntuacion_id FOREIGN KEY (puntuacion_id) REFERENCES public.lftragos_puntuacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_jornada lftragos_ali_jornada_id_4b180376a2fbae6f_fk_lftragos_jornada_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_jornada
    ADD CONSTRAINT lftragos_ali_jornada_id_4b180376a2fbae6f_fk_lftragos_jornada_id FOREIGN KEY (jornada_id) REFERENCES public.lftragos_jornada(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_alineacion_equipo lftragos_aline_equipo_id_6fd5403eee925c09_fk_lftragos_equipo_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_alineacion_equipo
    ADD CONSTRAINT lftragos_aline_equipo_id_6fd5403eee925c09_fk_lftragos_equipo_id FOREIGN KEY (equipo_id) REFERENCES public.lftragos_equipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_equipo lftragos_equipo_usuario_id_4942e123634c9888_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_equipo
    ADD CONSTRAINT lftragos_equipo_usuario_id_4942e123634c9888_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lftragos_puntuacion_jornada lftragos_pun_jornada_id_13cb786a2072ec40_fk_lftragos_jornada_id; Type: FK CONSTRAINT; Schema: public; Owner: txabi
--

ALTER TABLE ONLY public.lftragos_puntuacion_jornada
    ADD CONSTRAINT lftragos_pun_jornada_id_13cb786a2072ec40_fk_lftragos_jornada_id FOREIGN KEY (jornada_id) REFERENCES public.lftragos_jornada(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

